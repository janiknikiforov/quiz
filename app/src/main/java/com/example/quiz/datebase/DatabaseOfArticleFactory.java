package com.example.quiz.datebase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.quiz.model.ArticleModel;
import com.example.quiz.model.QuestionModel2;
import java.util.ArrayList;
import java.util.List;

public class DatabaseOfArticleFactory {
    private DatabaseOfArticleHelper databaseOfArticleHelper;
    public static SQLiteDatabase db;
    public static List<ArticleModel> articleList;
    private Cursor userCursor;

    @SuppressLint("Range")
    public DatabaseOfArticleFactory(Context c) {
        // создаем базу данных
        databaseOfArticleHelper = new DatabaseOfArticleHelper(c.getApplicationContext());
        databaseOfArticleHelper.create_db();
        db = databaseOfArticleHelper.open();
        articleList = new ArrayList<>();

        userCursor = db.rawQuery("select * from " + DatabaseOfArticleHelper.TABLE, null);
        while (userCursor.moveToNext()) {
            articleList.add(new ArticleModel(
                    userCursor.getInt(userCursor.getColumnIndex(DatabaseOfArticleHelper.COLUMN_ID)),
                    userCursor.getString(userCursor.getColumnIndex(DatabaseOfArticleHelper.COLUMN_Name)),
                    userCursor.getString(userCursor.getColumnIndex(DatabaseOfArticleHelper.COLUMN_TEXT))
            ));
        }
    }

    public List<ArticleModel> getList() {
        return articleList;
    }
}