package com.example.quiz.datebase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.quiz.model.QuestionModel2;

import java.util.ArrayList;
import java.util.List;

public class DatabaseOfQuestionFactory {
    private DatabaseOfQuestionHelper databaseOfQuestionHelper;
    public static SQLiteDatabase db;
    public static List<QuestionModel2> listAllQuestions;
    private Cursor userCursor;

    @SuppressLint("Range")
    public DatabaseOfQuestionFactory(Context c) {
        // создаем базу данных
        databaseOfQuestionHelper = new DatabaseOfQuestionHelper(c.getApplicationContext());
        databaseOfQuestionHelper.create_db();
        db = databaseOfQuestionHelper.open();
        listAllQuestions = new ArrayList<>();

        userCursor = db.rawQuery("select * from " + DatabaseOfQuestionHelper.TABLE, null);
        while (userCursor.moveToNext()) {
            listAllQuestions.add(new QuestionModel2(
                    userCursor.getInt(userCursor.getColumnIndex(DatabaseOfQuestionHelper.COLUMN_ID)),
                    userCursor.getString(userCursor.getColumnIndex(DatabaseOfQuestionHelper.COLUMN_QUESTION)),
                    userCursor.getString(userCursor.getColumnIndex(DatabaseOfQuestionHelper.COLUMN_ANSWER1)),
                    userCursor.getString(userCursor.getColumnIndex(DatabaseOfQuestionHelper.COLUMN_ANSWER2)),
                    userCursor.getString(userCursor.getColumnIndex(DatabaseOfQuestionHelper.COLUMN_ANSWER3))
                    //userCursor.getString(userCursor.getColumnIndex(DatabaseOfQuestionHelper.COLUMN_ANSWER4))
            ));
        }
    }

    public List<QuestionModel2> getList() {
        return listAllQuestions;
    }
}