package com.example.quiz.datebase;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.quiz.R;
import com.example.quiz.activity.ArticleActivity;
import com.example.quiz.activity.HolderActivity;
import com.example.quiz.activity.ItemClickListener;
import com.example.quiz.activity.MainActivity;
import com.example.quiz.model.ArticleModel;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<HolderActivity> implements Filterable {

    Context c;
    List<ArticleModel> articleList, filterList;
    CustomFilter filter;

    public MyAdapter(Context c, List<ArticleModel> articleList) {
        this.c = c;
        this.articleList = articleList;
        filterList = articleList;
    }

    @NonNull
    @Override
    public HolderActivity onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, null);
        return new HolderActivity(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderActivity holder, int position) {
        holder.nameOfArticle.setText(articleList.get(position).getName());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClickListener(View v, int position) {
                String id = articleList.get(position).getId() + "";
                String text = articleList.get(position).getText();
                Intent intent = new Intent(c, ArticleActivity.class);
                intent.putExtra("Id", id);
                intent.putExtra("Text", text);
                c.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilter(filterList, this);
        }
        return filter;
    }
}