package com.example.quiz.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.quiz.R;
import com.example.quiz.datebase.DatabaseOfArticleFactory;
import com.example.quiz.model.ArticleModel;
import java.util.ArrayList;
import java.util.List;

public class ArticleActivity extends AppCompatActivity implements View.OnClickListener {
    ArticleModel item;
    String id;
    TextView nameArticle, textArticle;
    Button btn;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        // set color in status bar
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(this.getResources().getColor(R.color.statusBarNew));

        btn = findViewById(R.id.continueBtn);
        btn.setOnClickListener(this);
        ImageView iv = findViewById(R.id.iconBackInArticleActivity);
        iv.setOnClickListener(this);

        nameArticle = findViewById(R.id.nameArticle);
        textArticle = findViewById(R.id.textArticle);

        intent = getIntent();
        id = intent.getStringExtra("Id");

        for(int i = 0; i < DatabaseOfArticleFactory.articleList.size(); i++)
        {
            if((DatabaseOfArticleFactory.articleList.get(i).getId() + "").equals(id))
            {
                item = DatabaseOfArticleFactory.articleList.get(i);

                if (i + 1 < DatabaseOfArticleFactory.articleList.size()) {
                    String nextName = DatabaseOfArticleFactory.articleList.get(i + 1).getName();
                    btn.setText("Следующая статья \"" + nextName + "\"");
                }
                break;
            }
        }

        nameArticle.setText(item.getName());
        textArticle.setText(item.getText());

        id = item.getId() + 1 + "";

        if (nameArticle.getText() == DatabaseOfArticleFactory.articleList.get(DatabaseOfArticleFactory.articleList.size() - 1).getName()){
            btn.setText("Вернуться на главный экран");
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.iconBackInArticleActivity) { onBackPressed(); return;}

        ScrollView scrollUp;
        scrollUp = (ScrollView) findViewById(R.id.scrollArticle);
        for(int i = 0; i < DatabaseOfArticleFactory.articleList.size(); i++)
        {
            if((DatabaseOfArticleFactory.articleList.get(i).getId() + "").equals(id))
            {
                item = DatabaseOfArticleFactory.articleList.get(i);

                if (i + 1 < DatabaseOfArticleFactory.articleList.size()) {
                    String nextName = DatabaseOfArticleFactory.articleList.get(i + 1).getName();
                    btn.setText("Следующая статья \"" + nextName + "\"");
                }
                break;
            }
        }

        nameArticle.setText(item.getName());
        textArticle.setText(item.getText());
        id = item.getId() + 1 + "";
        scrollUp.scrollTo(0, 0);

        if(btn.getText() == "Вернуться на главный экран")
        {
            finish();
            return;
        }

        if (nameArticle.getText() == DatabaseOfArticleFactory.articleList.get(DatabaseOfArticleFactory.articleList.size() - 1).getName()){
            btn.setText("Вернуться на главный экран");
        }
    }
}