package com.example.quiz.activity;

import android.view.View;

public interface ItemClickListener {
    void onItemClickListener(View v, int position);
}