package com.example.quiz.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.quiz.R;

import java.util.Timer;

public class BreathActivity extends AppCompatActivity implements View.OnClickListener {
    TextView action, tv;
    Timer timer;
    TextView sec4;
    TextView sec6;
    TextView sec8;
    androidx.cardview.widget.CardView tab;
    long t;
    boolean flag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breath);

        // set color in status bar
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(this.getResources().getColor(R.color.statusBarNew));


        action = (TextView) findViewById(R.id.action);
        tv = (TextView) findViewById(R.id.timer);

        sec4 = (TextView) findViewById(R.id.sec4);
        sec4.setOnClickListener(this);
        sec6 = (TextView) findViewById(R.id.sec6);
        sec6.setOnClickListener(this);
        sec8 = (TextView) findViewById(R.id.sec8);
        sec8.setOnClickListener(this);
        tab = findViewById(R.id.tab);
        tab.setOnClickListener(this);


        ImageView home = (ImageView) findViewById(R.id.iconBackInBreath);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BreathActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        //breath();
    }

    private void breathIn() {
        new CountDownTimer(t,990){

            @Override
            public void onTick(long l) {
                action.setText("вдох...");
                tv.setText("" + l/1000);
                System.out.println(l + " 32222");
            }

            @Override
            public void onFinish() {

            }
        }.start();

    }

    private void breathOyt() {
        new CountDownTimer(t,990){

            @Override
            public void onTick(long l) {
                action.setText("выдох...");
                tv.setText("" + l/1000);
                System.out.println(l + " 82222");
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sec4:
                t = 4001;
                breathIn();
                flag = false;
                break;
            case R.id.sec6:
                t = 6001;
                breathIn();
                flag = false;
                break;
            case R.id.sec8:
                t = 8001;
                breathIn();
                flag = false;
                break;
            case R.id.tab:
                if(flag){
                    breathIn();
                    flag = false;
                } else{
                    breathOyt();
                    flag = true;
                }

        }
    }
}