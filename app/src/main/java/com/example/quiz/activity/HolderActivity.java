package com.example.quiz.activity;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quiz.R;


public class HolderActivity extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView nameOfArticle;
    ItemClickListener itemClickListener;

    public HolderActivity(@NonNull View itemView) {
        super(itemView);

        this.nameOfArticle = itemView.findViewById(R.id.nameOfArticle);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClickListener(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic){
        this.itemClickListener = ic;
    }
}