package com.example.quiz.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.quiz.datebase.DatabaseOfArticleFactory;
import com.example.quiz.R;
import com.example.quiz.datebase.DatabaseOfQuestionFactory;

public class InfoActivity extends AppCompatActivity {

    Toolbar toolbar;

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        // set color in status bar
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(this.getResources().getColor(R.color.statusBarNew));


        ImageView home = (ImageView) findViewById(R.id.iconBackInInfo);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InfoActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


        TextView numberOfArticle = findViewById(R.id.numberOfArticle);
        numberOfArticle.setText("Всего статей: " + DatabaseOfArticleFactory.articleList.size());

        TextView numberOfQuestion = findViewById(R.id.numberOfQuestion);
        numberOfQuestion.setText("Всего вопросов: " + DatabaseOfQuestionFactory.listAllQuestions.size());

        TextView currentScore = findViewById(R.id.currentScore);

        int score = SplashActivity.currentScore;
        if(score < 22) currentScore.setText("Вы набрали " + score + " из 42 баллов. Вероятно, вы не очень довольны собой, периодически вас мучают сомнения. ");
        else if(score < 31) currentScore.setText("Вы набрали " + score + " из 42 баллов. Вы живете в полном согласии с собой, адекватно оцениваете все свои достоинства и недостатки, доверяете себе. ");
        else currentScore.setText("Вы набрали " + score + " из 42 баллов. У вас высокая самооценка, вы уверены в своих силах и способностях и довольны собой. ");

    }
}