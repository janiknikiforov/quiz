package com.example.quiz.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.quiz.R;
import com.example.quiz.datebase.DatabaseOfQuestionFactory;
import com.example.quiz.model.QuestionModel2;
import com.google.android.material.bottomsheet.BottomSheetDialog;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener {

    Button  btn_two, btn_three, btn_four;
    ImageView btn_back;
    TextView tv_question, tv_question_Attempted;


    int currentScore = 0, questionAttempted = 0, questionLength = DatabaseOfQuestionFactory.listAllQuestions.size();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        // set color in status bar
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(this.getResources().getColor(R.color.statusBarNew));

        btn_back = (ImageView)findViewById(R.id.iconBackInQuiz);
        btn_back.setOnClickListener(this);
        //btn_one = (Button)findViewById(R.id.btn_one);
        //btn_one.setOnClickListener(this);
        btn_two = (Button)findViewById(R.id.btn_two);
        btn_two.setOnClickListener(this);
        btn_three = (Button)findViewById(R.id.btn_three);
        btn_three.setOnClickListener(this);
        btn_four = (Button)findViewById(R.id.btn_four);
        btn_four.setOnClickListener(this);

        tv_question = (TextView)findViewById(R.id.tv_question);
        tv_question_Attempted = (TextView)findViewById(R.id.tv_question_Attempted);

        NextQuestion(questionAttempted);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            /*case R.id.btn_one:
            questionAttempted++;
                if(questionAttempted != questionLength){
                    currentScore += 1;
                    NextQuestion(questionAttempted);
                }else{
                    SplashActivity.currentScore = currentScore;
                    showBottomSheet();
                }
                break;*/

            case R.id.btn_two:
                questionAttempted++;
                if(questionAttempted != questionLength){
                    currentScore += 1;
                    NextQuestion(questionAttempted);
                }else{
                    currentScore += 1;
                    SplashActivity.currentScore = currentScore;
                    showBottomSheet();
                }
                break;

            case R.id.btn_three:
                questionAttempted++;
                if(questionAttempted != questionLength){
                    currentScore += 2;
                    NextQuestion(questionAttempted);
                }else{
                    currentScore += 2;
                    SplashActivity.currentScore = currentScore;
                    showBottomSheet();
                }
                break;

            case R.id.btn_four:
                questionAttempted++;
                if(questionAttempted != questionLength){
                    currentScore += 3;
                    NextQuestion(questionAttempted);
                }else{
                    currentScore += 3;
                    SplashActivity.currentScore = currentScore;
                    showBottomSheet();
                }
                break;
            case R.id.iconBackInQuiz:
                Intent intent = new Intent(QuizActivity.this, MainActivity.class);
                startActivity(intent);
                break;
            default:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    private void showBottomSheet(){

        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(QuizActivity.this);
        View bottomSheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.score_button_sheet,(LinearLayout) findViewById(R.id.LLScore));
        TextView scoreTV = bottomSheetView.findViewById((R.id.TVScore));
        Button restartQuiz = bottomSheetView.findViewById(R.id.Restart);
        Button backToHome = bottomSheetView.findViewById(R.id.backToHome);
        backToHome.setOnClickListener(this);

        if(currentScore < 22) scoreTV.setText("Вы набрали " + currentScore + " из 42 баллов. Вероятно, вы не очень довольны собой, периодически вас мучают сомнения. Скорее всего у вас высокие требования к себе, которые сопровождаются  неудовлетворенностью собственными способностями, действиями, внешностью. Вы зависимы от мнения других людей.  Из-за этого вам сложно найти себя в жизни и реализовать свои таланты. Пора это прекращать! И найти в себе те качества, которые вызывают уважение.  Принимайте себя в том уникальном образе, которым обладаете.");
        else if(currentScore < 31) scoreTV.setText("Вы набрали " + currentScore + " из 42 баллов. Вы живете в полном согласии с собой, адекватно оцениваете все свои достоинства и недостатки, доверяете себе. Это состояние можно назвать \"золотой серединой\". Вас сложно обидеть или убедить в обратном, если вы стоите на своем. Вы способны с легкостью найти оптимальное решение проблемы и выход из трудных ситуаций. У вас нормальная здоровая самооценка. К такому результату стремятся многие. ");
        else scoreTV.setText("Вы набрали " + currentScore + " из 42 баллов. У вас высокая самооценка, вы уверены в своих силах и способностях и довольны собой. Но при этом у вас есть потребность в доминировании. Вы часто критикуете других, но критику в свой адрес не воспринимаете. Многие удивляются вашей уверенности в себе и непробиваемости. Благодаря высокой самооценке, вы добиваетесь желаемого.");

        restartQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 startActivity(new Intent(getApplicationContext(), QuizActivity.class));
            }
        });

        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setContentView(bottomSheetView);
        bottomSheetDialog.show();
    }

    private void GameOver(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(QuizActivity.this);
        alertDialogBuilder
                .setMessage("Game Over")
                .setCancelable(false)
                .setPositiveButton("New Game", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getApplicationContext(), QuizActivity.class));
                    }
                })
                .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });
        alertDialogBuilder.show();

    }

    private void NextQuestion(int num){
        tv_question_Attempted.setText("Отвеченно вопросов : " + (questionAttempted + 1) + " / " + questionLength);
        tv_question.setText(DatabaseOfQuestionFactory.listAllQuestions.get(num).getQuestion());
        //btn_one.setText(DatabaseOfQuestionFactory.listAllQuestions.get(num).getAnswer1());
        btn_two.setText(DatabaseOfQuestionFactory.listAllQuestions.get(num).getAnswer1());
        btn_three.setText(DatabaseOfQuestionFactory.listAllQuestions.get(num).getAnswer2());
        btn_four.setText(DatabaseOfQuestionFactory.listAllQuestions.get(num).getAnswer3());
    }
}