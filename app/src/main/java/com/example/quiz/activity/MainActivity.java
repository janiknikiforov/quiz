package com.example.quiz.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.quiz.R;
import com.example.quiz.datebase.DatabaseOfArticleFactory;
import com.example.quiz.datebase.MyAdapter;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    RecyclerView mRecyclerView;
    MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set color in status bar
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(this.getResources().getColor(R.color.statusBarNew));

        mRecyclerView = findViewById(R.id.recycleView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new MyAdapter(this, DatabaseOfArticleFactory.articleList);
        mRecyclerView.setAdapter(myAdapter);

        findViewById(R.id.info).setOnClickListener(this);
        findViewById(R.id.search).setOnClickListener(this);
        findViewById(R.id.quiz).setOnClickListener(this);
        findViewById(R.id.breath).setOnClickListener(this);
        System.out.println("62222");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.info:
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(intent);
                break;
            case R.id.search:
                intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
            case R.id.quiz:
                System.out.println("52222");
                intent = new Intent(MainActivity.this, QuizActivity.class);
                startActivity(intent);
                break;
            case R.id.breath:
                System.out.println("42222");
                intent = new Intent(MainActivity.this, BreathActivity.class);
                startActivity(intent);
                break;
            default:
                Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
        }
    }
}
