package com.example.quiz.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;

import com.example.quiz.R;
import com.example.quiz.datebase.DatabaseOfArticleFactory;
import com.example.quiz.datebase.DatabaseOfQuestionFactory;

public class SplashActivity extends AppCompatActivity {
    static DatabaseOfArticleFactory articleFactory;
    static DatabaseOfQuestionFactory questionFactory;
    static int currentScore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        ActionBar actionBar = getSupportActionBar();
//        actionBar.hide();

        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(this.getResources().getColor(R.color.statusBarNew));

        questionFactory = new DatabaseOfQuestionFactory(this);
        articleFactory = new DatabaseOfArticleFactory(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }, 1100);
    }
}
